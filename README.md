# Knihovna pro komunikaci se systémy I6 od společnosti Cybersoft

## Instalace

### Composer

Pro instalaci balíčku je nutné jej instalovat skrze [composer](https://getcomposer.org/).

```bash
composer require strambm/cybersoft-i6-api
```

### Ukázka užití

## SynchronizaceProduktu

### ResultByCode

```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use CybersoftI6Api\CybersoftI6Api;
use CybersoftI6Api\Connection\Login;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Models\ResultTypes\StoItemBase as ReturnModel; //Pokud máte jiné nastavení u dodavatele, stačí předědit model a dát zde do use;

/*
    apiUrl = http://JMENO:HESLO@WWW.CYBERSOFT.CZ/i6ws/Default.asmx;
    urlApiDomain = 'http://WWW.CYBERSOFT.CZ'
*/
$login = new Login('username', 'password', 'urlApiDomain');

$api = new CybersoftI6Api($login);
$stoBaseItems = $api->call(ReturnModel::class, RequestMethod::BY_CODE, ['code' => '123456']);
$item = \current($stoBaseItems);
```

### Result

```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use CybersoftI6Api\CybersoftI6Api;
use CybersoftI6Api\Connection\Login;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Models\ResultTypes\StoItemBase as ReturnModel;

$login = new Login('username', 'password', 'urlApiDomain');

$api = new CybersoftI6Api($login);
$stoBaseItems = $api->call(ReturnModel::class, RequestMethod::GET_RESULT);
```

## Synchronizace Faktur

### ResultByFromTo

```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use CybersoftI6Api\CybersoftI6Api;
use CybersoftI6Api\Connection\Login;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Models\ResultTypes\DocTrInv as ReturnModel;
use Carbon\Carbon;

$login = new Login('username', 'password', 'urlApiDomain');

$api = new CybersoftI6Api($login);
$stoBaseItems = $api->call(ReturnModel::class, RequestMethod::BY_FROM_TO, ['from' => Carbon::today()->subDays(14), 'to' => Carbon::today()]);
```

### Result

Nevrátí Vám výpis všech faktur, ale jen všech faktur z daného dne.

```php
<?php
require_once __DIR__ . '/vendor/autoload.php';

use CybersoftI6Api\CybersoftI6Api;
use CybersoftI6Api\Connection\Login;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Models\ResultTypes\DocTrInv as ReturnModel;

$login = new Login('username', 'password', 'urlApiDomain');

$api = new CybersoftI6Api($login);
$stoBaseItems = $api->call(ReturnModel::class, RequestMethod::GET_RESULT);
```
