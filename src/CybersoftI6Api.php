<?php
namespace CybersoftI6Api;

use CybersoftI6Api\Models\Model;
use CybersoftI6Api\Connection\Login;
use CybersoftI6Api\Connection\RequestMethod;

class CybersoftI6Api
{
    protected $login;
    protected $schemas = [];
    
    public function __construct(Login $loginInfo)
    {
        $this->login = $loginInfo;
    }

    public function call(string $model, string $method, array $filterProps = []): array
    {
        if((new $model() instanceof Model)) {
            $model::setLogin($this->login);
            $methodName = \lcfirst($method);
            switch($method) {
                case RequestMethod::GET_RESULT:
                    return $model::$methodName();
                case RequestMethod::BY_CODE:
                    return $model::$methodName($filterProps['code']);
                case RequestMethod::BY_FROM_TO:
                    return $model::$methodName($filterProps['from'], $filterProps['to']);
            }
        }
    }
}
