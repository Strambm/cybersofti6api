<?php
namespace CybersoftI6Api\Models;

use Carbon\Carbon;
use CybersoftI6Api\Connection\Login;
use CybersoftI6Api\Connection\Request;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Exceptions\UnimplementedException;
use CybersoftI6Api\Exceptions\RequestMethodDoesNotExist;
use CybersoftI6Api\Exceptions\RequestMethodIsUnavailable;

abstract class Model 
{
    protected static $login;

    protected $attributes = [];

    public abstract static function getDescription(): string;
    public abstract static function getAllowedHours(string $method): array;
    protected abstract static function getResultTypeName(): string;
    protected abstract static function parseXml(\XMLReader $xml): array;
    protected abstract static function getXmlObjectName(): string;


    public function __get(string $attributeName)
    {
        $mutateAttributeName = static::mutateAttributeName($attributeName);
        
        $mutateMethod = self::mutateMethod(__FUNCTION__, $attributeName);
        if(\method_exists($this, $mutateMethod))
        {
            return $this->$mutateMethod();
        }
        $mutateMethod = self::mutateMethod(__FUNCTION__, $mutateAttributeName);
        if(\method_exists($this, $mutateAttributeName))
        {
            return $this->$mutateMethod();
        }

        return $this->getAttributeValue($attributeName);
    }

    protected static final function mutateAttributeName(string $attributeName): string
    {
        return \strtolower($attributeName);
    }

    private static final function mutateMethod(string $magicFunction, string $attributeName): string
    {
        switch($magicFunction) {
            case '__get':
                $prefix = 'get';
                break;
            default:
                throw new UnimplementedException();
        }
        $mutateMethod = $prefix . \ucfirst($attributeName) . 'Attribute';
        return $mutateMethod;
    }

    public function getAttributeValue(string $attributeName)
    {
        $mutateAttributeName = static::mutateAttributeName($attributeName);
        if(\array_key_exists($mutateAttributeName, $this->attributes)) {
            return $this->attributes[$mutateAttributeName];
        }
    }

    protected static function throwRequestMethodDoesNotExist(string $method): void
    {
        throw new RequestMethodDoesNotExist('Request method \'' . $method . '\' does not exist!');
    }

    public static function isAllowedRequest(string $method): bool
    {
        try {
            $allowedHours = static::getAllowedHours($method);
            $now = Carbon::now();
            return \in_array($now->hour, $allowedHours);
        } catch(RequestMethodDoesNotExist $exception) {
            return false;
        }
    }

    public static function setLogin(Login $login): void
    {
        self::$login = $login;
    }

    public static function getResult(): array
    {
        $method = RequestMethod::GET_RESULT;
        return static::getResultHandle($method);
    }

    public static function GetResultByCode(string $code): array
    {
        $method = RequestMethod::BY_CODE;
        return static::getResultHandle($method, [
            'code' => $code,
        ]);
    }

    public static function GetResultByFromTo(Carbon $from, Carbon $to): array
    {
        $method = RequestMethod::BY_FROM_TO;
        return static::getResultHandle($method, [
            'from' => $from->format('Y-m-d'), 
            'to' => $to->format('Y-m-d'),
        ]);
    }

    private static function getResultHandle(string $method, array $params = []): array
    {
        $params['resultType'] = static::getResultTypeName();
        if(static::isAllowedRequest($method)) {
            $request = new Request(self::$login, $method, $params);
            $request->handle();
            $xml = $request->getXmlContent();
            return static::parseXml($xml);
        } else {
            throw new RequestMethodIsUnavailable('Method \'' . $method . '::resultType=' . static::getResultTypeName() . '\' is allowed in hours (' . \implode(', ', static::getAllowedHours($method)) . ')');
        }
    }
}
