<?php
namespace CybersoftI6Api\Models;

use CybersoftI6Api\Exceptions\NothingToParsing;

abstract class ModelWithAttributes extends Model
{
    public abstract static function getSchema();

    protected static function parseXml(\XMLReader $xml): array
    {
        $returnArray = [];
        while($xml->read()) {
            if($xml->name == static::getXmlObjectName() && $xml->nodeType == \XMLReader::ELEMENT) {
                $instance = static::prepaireAttributesFromXml($xml);
                $returnArray[] = $instance;
            } elseif($xml->nodeType == \XMLReader::ELEMENT) {
                $resultAttributes[$xml->name] = $xml->readInnerXml();
            }
        }
        return $returnArray;
    }

    private static function prepaireAttributesFromXml(\XMLReader &$xml): Model
    {
        $class = static::class;
        $model = new $class();
        $schema = static::getSchema();
        foreach($schema[static::getXmlObjectName()] as $paramName) {
            $model->attributes[static::mutateAttributeName($paramName)] = $xml->getAttribute($paramName);
        }
        while($xml->read()) {
            if($xml->name == static::getXmlObjectName() && $xml->nodeType == \XMLReader::END_ELEMENT) {
                return $model;
            } elseif(\array_key_exists($xml->name, $schema) && $xml->nodeType == \XMLReader::ELEMENT) {
                foreach($schema[$xml->name] as $paramName) {
                    $tmpStruct[static::mutateAttributeName($paramName)] = $xml->getAttribute($paramName);
                }
                $model->attributes[\strtolower($xml->name)][] = $tmpStruct;
            }
        }
    }
}
