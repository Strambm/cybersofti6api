<?php
namespace CybersoftI6Api\Models\Enum;

class DocumentType
{
    const CORRECTION_INVOICE = 67;
    const INVOICE = 73;
    const PROFORMA_INVOICE = 80;
    const PENALTY_INVOICE = 89;
    const JSD_INVOICE = 106;
    const REVERSE_CHARGE_INVOICE = 107;
    const REVERSE_CHARGE_CORRECTION_INVOICE = 108;
}
