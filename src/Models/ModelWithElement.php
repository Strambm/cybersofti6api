<?php
namespace CybersoftI6Api\Models;

use CybersoftI6Api\Exceptions\NothingToParsing;

abstract class ModelWithElement extends Model
{
    protected static function parseXml(\XMLReader $xml): array
    {
        $returnArray = [];
        $resultAttributes = [];
        while($xml->read()) {
            if($xml->name == 'Result') {
                continue;
            }
            if($xml->name == static::getXmlObjectName() && $xml->nodeType == \XMLReader::ELEMENT) {
                $instance = static::prepaireAttributesFromXml($xml);
                $instance->attributes['resultattributes'] = &$resultAttributes;
                $returnArray[] = $instance;
            } elseif($xml->nodeType == \XMLReader::ELEMENT) {
                $resultAttributes[$xml->name] = $xml->readInnerXml();
            }
        }
        return $returnArray;
    }

    private static function prepaireAttributesFromXml(\XMLReader &$xml): Model
    {
        $class = static::class;
        $model = new $class();
        while($xml->read()) {
            if($xml->name == static::getXmlObjectName() && $xml->nodeType == \XMLReader::END_ELEMENT) {
                return $model;
            } elseif($xml->nodeType == \XMLReader::ELEMENT) {
                $model->attributes[static::mutateAttributeName($xml->name)] = $xml->readInnerXml();
            }
        }
    }
}
