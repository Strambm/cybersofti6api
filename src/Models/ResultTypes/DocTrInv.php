<?php
namespace CybersoftI6Api\Models\ResultTypes;

use CybersoftI6Api\Models\Entity\Currency;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Models\ModelWithAttributes;

/**
 * @see \CybersoftI6Api\Models\Enum\DocumentType
 */
class DocTrInv extends ModelWithAttributes
{
    public static function getDescription(): string
    {
        return 'Document Transfer - Invoice (Format for import into I6).';
    }

    public static function getAllowedHours(string $method): array
    {
        switch($method) {
            case RequestMethod::GET_RESULT:
                return \range(0, 23);
            case RequestMethod::BY_CODE:
                return \range(0, 23);
            case RequestMethod::BY_FROM_TO:
                return \range(0, 23);
        }
        static::throwRequestMethodDoesNotExist($method);
    }

    protected static function getResultTypeName(): string
    {
        return 'DocTrInv';
    }

    protected static function getXmlObjectName(): string
    {
        return 'Invoice';
    }

    public static function getSchema()
    {
        return [
            'Invoice' => [
                'Id',
                'Code',
                'ZCode',
                'CodeO',
                'SymCon',
                'OrdId',
                'OrdCode',
                'OrdCodeO',
                'OrdC',
                'Type',
                'DateAcc',
                'DateDue',
                'CurCode',
                'Val',
                'ValCur',
                'ValRnd',
                'ValRndCur',
                'ValPaid',
                'ValPaidCur',
                'ZVal',
                'C',
            ],
            'InvItem' => [
                'Id',
                'StiId',
                'StiCode',
                'StiCode2',
                'StiPartNo',
                'StiPartNo2',
                'StiEAN',
                'StiEAN2',
                'StiName',
                'Name',
                'OrdId',
                'OrdCode',
                'OrdCodeO',
                'OriC',
                'Qty',
                'TaxRate',
                'Prc',
                'PrcTax',
                'CurCode',
                'PrcCur',
                'PrcTaxCur',
                'PrcRefCur',
                'RefCode',
                'RefProCode',
                'PrcRefCur2',
                'RefCode2',
                'RefProCode2',
                'ZPrc',
                'ZPrcTax',
                'ZPrcRef',
                'ZPrcRef2',
                'RefIs',
            ],
            'Delivery' => [
                'OrdId',
                'Code',
                'CstName',
                'CstStreet',
                'CstCity',
                'CstPostCode',
                'CstCouName',
            ],
            'Order' => [
                'Id',
                'Code',
                'C',
                'CstName',
                'CstStreet',
                'CstCity',
                'CstPostCode',
                'CstCouName',
            ],
        ];
    }

    public function getBasketItemsAttribute(?int $ordId = null): array
    {
        return array_filter(
            $this->invItem,
            function(array $basketRow) use ($ordId): bool
            {
                $result = is_null($basketRow['refis']);
                if(!is_null($ordId)) {
                    $result = $result && $basketRow['ordid'] == $ordId;
                }
                return $result;
            }
        );
    }

    public function getCurrencyAttribute(): Currency
    {
        return new Currency($this->curCode);
    }
}
