<?php
namespace CybersoftI6Api\Models\ResultTypes;

use CybersoftI6Api\Models\ModelWithElement;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Models\Entity\Currency;
use CybersoftI6Api\Models\ResultTypes\Interfaces\StoItem;


class StoItemBase extends ModelWithElement implements StoItem
{
    public static function getDescription(): string
    {
        return 'Base informations about products.';
    }

    public static function getAllowedHours(string $method): array
    {
        switch($method) {
            case RequestMethod::GET_RESULT:
                return [1, 2, 3, 4, 5, 6, 7, 21, 22, 23, 0];
            case RequestMethod::BY_CODE:
                return \range(0, 23);
            case RequestMethod::BY_FROM_TO:
                return [1, 2, 3, 4, 5, 6, 7, 21, 22, 23, 0];
        }
        static::throwRequestMethodDoesNotExist($method);
    }

    protected static function getResultTypeName(): string
    {
        return 'StoItemBase_El';
    }

    protected static function getXmlObjectName(): string
    {
        return 'StoItem';
    }

    public function getUrlBaseAttribute(): string
    {
        $id = $this->id;
        $url = $this->resultAttributes['UrlBase'];
        $urlBase = $url . $id;
        return $urlBase;
    }

    public function getQtyFreeAttribute(): int
    {
        return (int) $this->getAttributeValue('QtyFree');
    }

    public function getPriceWithoutVatAttribute(): float
    {
        return $this->getAttributeValue('PriceDea') + $this->getAttributeValue('PriceRef');
    }

    public function getCurrencyAttribute(): Currency
    {
        return new Currency($this->resultAttributes['CouCode']);
    }
}
