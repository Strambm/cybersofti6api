<?php
namespace CybersoftI6Api\Models\ResultTypes\Interfaces;

interface StoItem
{
    public function getQtyFreeAttribute(): int;
    public function getPriceWithoutVatAttribute(): float;
    public function getUrlBaseAttribute(): string;
}
