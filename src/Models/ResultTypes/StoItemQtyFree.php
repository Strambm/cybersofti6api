<?php
namespace CybersoftI6Api\Models\ResultTypes;

use CybersoftI6Api\Models\ModelWithElement;
use CybersoftI6Api\Connection\RequestMethod;
use CybersoftI6Api\Exceptions\ModelAttributeIsUnvailable;

class StoItemQtyFree extends ModelWithElement
{
    public static function getDescription(): string
    {
        return 'Products on store.';
    }

    public static function getAllowedHours(string $method): array
    {
        switch($method) {
            case RequestMethod::GET_RESULT:
                return \range(0, 23);
            case RequestMethod::BY_CODE:
                return [1, 2, 3, 4, 5, 6, 22, 23, 0];
            case RequestMethod::BY_FROM_TO:
                return [];
        }
        static::throwRequestMethodDoesNotExist($method);
    }

    protected static function getResultTypeName(): string
    {
        return 'StoItemQtyFree_El';
    }

    protected static function getXmlObjectName(): string
    {
        return 'StoItem';
    }

    public function getQtyFreeAttribute(): int
    {
        return (int) $this->getAttributeValue('QtyFree');
    }

    public function getPriceWithoutVatAttribute(): float
    {
        throw new ModelAttributeIsUnvailable('Method \'' . __FUNCTION__ . '\' is unvailable for resultType \'' . static::class . '\'');
    }

    public function getUrlBaseAttribute(): string
    {
        throw new ModelAttributeIsUnvailable('Method \'' . __FUNCTION__ . '\' is unvailable for resultType \'' . static::class . '\'');
    }
}
