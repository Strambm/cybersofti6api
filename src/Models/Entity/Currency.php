<?php

namespace CybersoftI6Api\Models\Entity;

use CybersoftI6Api\Exceptions\UnimplementedException;

class Currency
{
    const ALLOWED_CODES = [
        'EUR' => 'EUR',
        'SK' => 'EUR',
        'CZ' => 'CZK',
        'CZK' => 'CZK',
    ];
    protected $code;

    public function __construct(string $code)
    {
        $this->code = $this->translateCodeToISO($code);
    }

    private function translateCodeToISO(string $currencyCodeCybersoft): string
    {
        if (\array_key_exists($currencyCodeCybersoft, static::ALLOWED_CODES)) {
            return static::ALLOWED_CODES[$currencyCodeCybersoft];
        }
        throw new UnimplementedException('Missing implementation for currency code \'' . $currencyCodeCybersoft . '\' in class \'' . static::class . '\'');
    }

    public function __get(string $attributeName)
    {
        return $this->$attributeName;
    }
}
