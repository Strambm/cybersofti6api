<?php
namespace CybersoftI6Api\Connection;

use CybersoftI6Api\Connection\Login;
use stdClass;

class Request 
{   
    protected $login;
    protected $requestInputs;
    protected $requestMethod;
    protected $content = null;
    
    public function __construct(Login $login, string $requestMethod, array $requestInputs = [])
    {
        $this->login = $login;
        $this->requestMethod = $requestMethod;
        $this->requestInputs = $requestInputs;
    }

    public function handle(): void
    {
        $soap = $this->getSoap();
        $requestMethod = $this->requestMethod;
        $result = $soap->$requestMethod($this->requestInputs);
        $this->storeContent($result);
    }

    private function getSoap(): \SoapClient
    {
        $login = [
            'login' => $this->login->username,
            'password' => $this->login->password,
        ];
        $url = $this->login->url . '/i6ws/Default.asmx?wsdl';
        return new \SoapClient($url, $login);
    }

    private function storeContent(stdClass $soapResult): void
    {
        $attributeName = $this->requestMethod . 'Result';
        $this->content = $soapResult->$attributeName->any;
    }
    
    public function getContent(): string
    {
        return (string) $this->content;
    }
    
    public function getXmlContent() : \XMLReader
    {
        $xml = new \XMLReader();
        $xml->XML($this->content);
        return $xml;
    }
    
    public function toFile(string $filename): void
    {
        $this->makeEmptyFile($filename);
        \file_put_contents($filename, $this->getContent());
    }
    
    private function makeEmptyFile(string $filename) : void
    {
        \fopen($filename, 'w');
    }
}
