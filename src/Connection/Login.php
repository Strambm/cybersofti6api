<?php
namespace CybersoftI6Api\Connection;

class Login 
{
    public $password;
    public $username;
    public $url;
    
    public function __construct(string $username, string $password, string $url)
    {
        $this->password = $password;
        $this->username = $username;
        $this->url = $url;
    }

    public function getConnectionString() : string
    {
        return $this->username . ':' . $this->password;
    }
}
