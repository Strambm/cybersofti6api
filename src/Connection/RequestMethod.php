<?php
namespace CybersoftI6Api\Connection;

class RequestMethod
{
    const GET_RESULT = 'GetResult';
    const BY_CODE = 'GetResultByCode';
    const BY_FROM_TO = 'GetResultByFromTo';
}
